const Task = require("../models/task.js");
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name,
  });
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return task;
    }
  });
};

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return removedTask;
    }
  });
};

module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      // console.log(newContent.name);

      if (newContent.name != null) {
        result.name = newContent.name;
      }

      if (newContent.status != null) {
        result.status = newContent.status;
      }

      return result.save().then((updatedTask, saveErr) => {
        if (saveErr) {
          console.log(saveErr);
          return false;
        } else {
          return updatedTask;
        }
      });
    }
  });
};

module.exports.getSpecificTasks = (taskId) => {
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return result;
    }
  });
};

module.exports.updateTaskStatus = (taskId, newContent) => {
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      result.status = newContent.status;
      return result.save().then((updatedTaskStatus, saveErr) => {
        if (saveErr) {
          console.log(saveErr);
          return false;
        } else {
          return updatedTaskStatus;
        }
      });
    }
  });
};
