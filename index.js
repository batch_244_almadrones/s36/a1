// set up dependecies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes.js");

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-course-booking.rtqczgx.mongodb.net/b244-to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
app.use("/tasks", taskRoute);
app.listen(port, () => console.log(`Now listening to port ${port}`));
